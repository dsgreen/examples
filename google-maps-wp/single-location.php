<?php
/**
 * The Template for displaying all single locations.
 * based on single.php
 * 
 * @package project
 */

get_header('map'); ?>

<script type="text/javascript">

    var activeModal = '';
    
    var stylesArray = [
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 },
                { "lightness": -8 },
                { "gamma": 1.18 },
                { "visibility": "simplified" }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 },
                { "gamma": 0.4 },
                { "lightness": -20 },
                { "visibility": "simplified" }
            ]
        },
        
        { "featureType": "road.local", "stylers": [ { "visibility": "simplified" } ] },
        
        {
            "featureType": "road",
            "stylers": [
                { "saturation": -100 },
                { "lightness": 20 },
                { "visibility": "simplified" }
            ]
        },

        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "off" }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                { "visibility": "off" }
            ]
        },
        {
            "featureType": "transit",
            "stylers": [
                { "visibility": "off" }
            ]
        },
        {
            "featureType": "water",
            "stylers": [
                { "saturation": 40 },
                { "hue": -10 },
                { "lightness": -7 },
                { "visibility": "on" },
                { "color": "#e2ebf4" }
            ]
        },
        {
            "featureType": "administrative",
            "stylers": [
                { "saturation": -100 },
                { "lightness": 25 },
                { "visibility": "on" }
            ]
        },
        {
            "featureType": "landscape",
            "stylers": [
                { "saturation": -100 },
                { "lightness": 20 },
                { "gamma": 1.5 },
                { "visibility": "simplified" },
                { "color": "#e6e3d7" }
            ]
        }
    ];
    function initialize() {
        var startLatLon = new google.maps.LatLng(44.565070, -69.660732);
        var mapOptions = {
            center: startLatLon,
            zoom: 9,
            maxZoom: 15,
            minZoom: 8,
            streetViewControl: false,
            rotateControl: false,
            panControl: false,
            mapTypeControl: false,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            styles: stylesArray
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var geocoder = new google.maps.Geocoder();
        var image = {
            url: '<?php echo get_template_directory_uri(); ?>/images/marker.png',
            scaledSize: new google.maps.Size(100,80)
        };

<?php while ( have_posts() ) : the_post(); ?>

        var marker = new google.maps.Marker({
            map: map,
            icon: image,
            title: "<?php the_title(); ?>",
            position: new google.maps.LatLng<?php echo get_geocode_latlng( $post->ID ); ?>
        });
        
        var contentString = '<?php 
        if (get_field('image_size') == 'Tall') {
            if ( has_post_thumbnail() ) { the_post_thumbnail('tall', array('class' => 'tall-img')); }
        }
        elseif (get_field('image_size') == 'Wide') {
            if ( has_post_thumbnail() ) { the_post_thumbnail('wide', array('class' => 'wide-img')); }
        }
        else {
            if ( has_post_thumbnail() ) { the_post_thumbnail('medium', array('class' => 'sm-img')); }
        }
        ?>'+
        '<h2 class="title-location"><?php the_title(); ?></h2>'+
        '<p class="address"><?php the_field('address_1'); ?><br>'+
        '<?php if (get_field('address_2') != '') { the_field('address_2'); echo '<br>'; } ?>'+
        '<?php the_field('city'); echo ', '; the_field('state'); echo ' '; the_field('zip'); ?><br>'+
        '<?php if (get_field('phone') != '') { the_field('phone'); echo '<br>'; } ?>'+
        '<?php // if (get_field('web') != '') { echo '<a href="'; the_field('web'); echo '">'; the_field('web'); echo '</a>'; } ?></p>'+

        '<p class="more" id="more" onclick="activeModal=\'modal\';document.getElementById(\'modal\').setAttribute(\'class\',\'lg_box visible\');document.getElementById(\'backdrop\').setAttribute(\'class\',\'modal-backdrop visible\');"><a href="#">Read more &raquo;</a></p>';

        var infoBox = new InfoBox({
            content: contentString
            ,alignBottom: true
            ,boxClass: 'info_box'
            ,closeBoxURL: '<?php echo get_template_directory_uri(); ?>/images/close-btn.png'
            ,infoBoxClearance: new google.maps.Size(60, 120)
            ,pixelOffset: new google.maps.Size(-200, -90)
        });
        google.maps.event.addListener(marker, "click", function(e) {
            infoBox.open(map, this);
        });

<?php endwhile; // end of the loop. ?>
    
    } // initialize
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-canvas"></div>

<?php
    while ( have_posts() ) : the_post(); 

    // info box
    get_template_part( 'content', 'location' );

    endwhile; // end of the loop. 
?>

<div class="modal-backdrop hidden" id="backdrop"></div>
<?php get_footer('map'); ?>