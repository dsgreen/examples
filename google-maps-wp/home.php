<?php
/**
 * The Template for displaying the Map.
 * @package project
 */
get_header('map');
?>
<script type="text/javascript">

    var infoWindows = new Array();
    var activeModal = '';
    
    var stylesArray = [
        {
            "featureType": "road.highway",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 },
                { "lightness": -8 },
                { "gamma": 1.18 },
                { "visibility": "simplified" }
            ]
        },
        {
            "featureType": "road.arterial",
            "elementType": "geometry",
            "stylers": [
                { "saturation": -100 },
                { "gamma": 0.4 },
                { "lightness": -20 },
                { "visibility": "simplified" }
            ]
        },
        
        { "featureType": "road.local", "stylers": [ { "visibility": "simplified" } ] },
        
        {
            "featureType": "road",
            "stylers": [
                { "saturation": -100 },
                { "lightness": 20 },
                { "visibility": "simplified" }
            ]
        },

        {
            "featureType": "poi",
            "elementType": "geometry",
            "stylers": [
                { "visibility": "off" }
            ]
        },
        {
            "featureType": "poi",
            "stylers": [
                { "visibility": "off" }
            ]
        },
        {
            "featureType": "transit",
            "stylers": [
                { "visibility": "off" }
            ]
        },
        {
            "featureType": "water",
            "stylers": [
                { "saturation": 40 },
                { "hue": -10 },
                { "lightness": -7 },
                { "visibility": "on" },
                { "color": "#e2ebf4" }
            ]
        },
        {
            "featureType": "administrative",
            "stylers": [
                { "saturation": -100 },
                { "lightness": 25 },
                { "visibility": "on" }
            ]
        },
        {
            "featureType": "landscape",
            "stylers": [
                { "saturation": -100 },
                { "lightness": 20 },
                { "gamma": 1.5 },
                { "visibility": "simplified" },
                { "color": "#e6e3d7" }
            ]
        }
    ];
    function initialize() {
        var startLatLon = new google.maps.LatLng(44.565070, -69.660732);
        var mapOptions = {
            center: startLatLon,
            zoom: 9,
            maxZoom: 15,
            minZoom: 8,
            streetViewControl: false,
            rotateControl: false,
            panControl: false,
            mapTypeControl: false,
            zoomControlOptions: {
                style: google.maps.ZoomControlStyle.LARGE,
                position: google.maps.ControlPosition.RIGHT_CENTER
            },
            styles: stylesArray
        };
        var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        var geocoder = new google.maps.Geocoder();
        var image = {
            url: '<?php echo get_template_directory_uri(); ?>/images/marker.png',
            scaledSize: new google.maps.Size(100,80)
        };
<?php
// from cms
$i=0;
while ( have_posts() ) : the_post(); 
?>
        var marker<?php echo $i; ?> = new google.maps.Marker({
            map: map,
            icon: image,
            title: "<?php the_title(); ?>",
            position: new google.maps.LatLng<?php echo get_geocode_latlng( $post->ID ); ?>
        });
        
        var contentString<?php echo $i; ?> = '<?php 
        if (get_field('image_size') == 'Tall') {
            if ( has_post_thumbnail() ) { the_post_thumbnail('tall', array('class' => 'tall-img')); }
        }
        elseif (get_field('image_size') == 'Wide') {
            if ( has_post_thumbnail() ) { the_post_thumbnail('wide', array('class' => 'wide-img')); }
        }
        else {
            if ( has_post_thumbnail() ) { the_post_thumbnail('medium', array('class' => 'sm-img')); }
        }
        ?>'+
        '<h2 class="title-location"><?php the_title(); ?></h2>'+
        '<p class="address"><?php the_field('address_1'); ?><br>'+
        '<?php if (get_field('address_2') != '') { the_field('address_2'); echo '<br>'; } ?>'+
        '<?php the_field('city'); echo ', '; the_field('state'); echo ' '; the_field('zip'); ?><br>'+
        '<?php if (get_field('phone') != '') { the_field('phone'); echo '<br>'; } ?>'+
        '<?php // if (get_field('web') != '') { echo '<a href="'; the_field('web'); echo '">'; the_field('web'); echo '</a>'; } ?></p>'+

        '<p class="more" id="more<?php echo $i; ?>" onclick="activeModal=\'modal<?php echo $i; ?>\';document.getElementById(\'modal<?php echo $i; ?>\').setAttribute(\'class\',\'lg_box visible\');document.getElementById(\'backdrop\').setAttribute(\'class\',\'modal-backdrop visible\');"><a href="#">Read more &raquo;</a></p>';

        var infoBox<?php echo $i; ?> = new InfoBox({
            content: contentString<?php echo $i; ?>
            ,alignBottom: true            
            ,boxClass: 'info_box'
            ,closeBoxURL: '<?php echo get_template_directory_uri(); ?>/images/close-btn.png'
            ,infoBoxClearance: new google.maps.Size(60, 120)
            ,pixelOffset: new google.maps.Size(-200, -90)
        });
        infoWindows.push(infoBox<?php echo $i; ?>);

        google.maps.event.addListener(marker<?php echo $i; ?>, "click", function(e) {
            close_popups();
            infoBox<?php echo $i; ?>.open(map, this);
        });
<?php
++$i;
endwhile; // end of the loop.
?>
        function close_popups() {
          for(var i = 0; i < infoWindows.length; i++) {
            infoWindows[i].close();
          }
        }
    } // initialize
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<div id="map-canvas"></div>
<?php
$i=0;
while ( have_posts() ) : the_post(); 
?>

<div class="lg_box hidden" id="modal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="inner">
        <div class="content">
            <span class="glyphicon glyphicon-remove close_btn" onclick="document.getElementById('modal<?php echo $i; ?>').setAttribute('class','lg_box hidden');document.getElementById('backdrop').setAttribute('class','modal-backdrop hidden');"></span>
            <div class="row">
                <?php if (get_field('image_size') == 'Wide') {
                    echo '<div class="col-md-12">';
                }
                elseif (get_field('image_size') == 'Tall') {
                    echo '<div class="col-md-5 col-md-offset-1 pull-right">';
                }
                else {
                    echo '<div class="col-md-6 pull-right">';
                }; ?>
                    <div class="wrap">
                        <?php if ( has_post_thumbnail() ) { the_post_thumbnail('large', array('class' => 'lg-img img-responsive')); } ?>
                    </div>
                    <p class="small"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></p>
                </div>
                <?php if (get_field('image_size') == 'Wide') {
                    echo '<div class="col-md-12">';
                } else {
                    echo '<div class="col-md-6 pull-left">';
                }; ?>
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_field('address_1'); ?><br>
                    <?php if (get_field('address_2') != '') { the_field('address_2'); echo '<br>'; } ?>
                    <?php the_field('city'); echo ', '; the_field('state'); echo ' '; the_field('zip'); ?><br>
                    <?php if (get_field('phone') != '') { the_field('phone'); echo '<br>'; } ?>
                    <?php if (get_field('web') != '') { echo '<a href="'; the_field('web'); echo '">'; the_field('web'); echo '</a>'; } ?>
                    </p>
                    <?php if (get_field('hours') != '') { echo '<p>'; the_field('hours'); echo '</p>'; } ?>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="closer" onclick="document.getElementById(activeModal).setAttribute('class','lg_box hidden');document.getElementById('backdrop').setAttribute('class','modal-backdrop hidden');"></div>
</div>

<?php
++$i;
endwhile; // end of the loop.
?>

<div class="lg_box" id="notice" tabindex="-1" role="dialog">
    <div class="inner">
        <div class="content">
            <span class="glyphicon glyphicon-remove close_btn" onclick="document.getElementById('notice').setAttribute('class','lg_box hidden');document.getElementById('backdrop').setAttribute('class','modal-backdrop hidden');"></span>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <h2 class="text-center">Heading</h2>
                    <div class="copy">
                        <p>Intro</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1 text-center">
                    <img src="<?php echo get_template_directory_uri(); ?>/images/marker.png" alt="marker" width="100" height="80" />
                </div>
            </div>
        </div>
    </div>
    <div class="closer" onclick="document.getElementById('notice').setAttribute('class','lg_box hidden');document.getElementById('backdrop').setAttribute('class','modal-backdrop hidden');"></div>
</div>

<div class="modal-backdrop" id="backdrop"></div>

<script type="text/javascript">
var allCookies = document.cookie;
var pos = allCookies.indexOf('notice=');
if (pos != -1) {
    document.getElementById('notice').setAttribute('class','lg_box hidden');
    document.getElementById('backdrop').setAttribute('class','modal-backdrop hidden');
}
else {
    var expiry = new Date();
    expiry.setFullYear(expiry.getFullYear() + 1);
    document.cookie = 'notice=1;expires=' + expiry.toGMTString(); 
}
</script>
<?php get_footer('map'); ?>