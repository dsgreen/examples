<?php
/**
 * @package project
 * 
 * for displaying individual locations
 * based on content-single
 * 
 */
?>
<div class="lg_box hidden" id="modal<?php echo $i; ?>" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="inner">
        <div class="content">
            <span class="glyphicon glyphicon-remove close_btn" onclick="document.getElementById('modal<?php echo $i; ?>').setAttribute('class','lg_box hidden');document.getElementById('backdrop').setAttribute('class','modal-backdrop hidden');"></span>
            <div class="row">
                <?php if (get_field('image_size') == 'Wide') {
                    echo '<div class="col-md-12">';
                }
                elseif (get_field('image_size') == 'Tall') {
                    echo '<div class="col-md-5 col-md-offset-1 pull-right">';
                }
                else {
                    echo '<div class="col-md-6 pull-right">';
                }; ?>
                    <div class="wrap">
                        <?php if ( has_post_thumbnail() ) { the_post_thumbnail('large', array('class' => 'lg-img img-responsive')); } ?>
                    </div>
                    <p class="small"><?php echo get_post(get_post_thumbnail_id())->post_excerpt; ?></p>
                </div>
                <?php if (get_field('image_size') == 'Wide') {
                    echo '<div class="col-md-12">';
                } else {
                    echo '<div class="col-md-6 pull-left">';
                }; ?>
                    <h2><?php the_title(); ?></h2>
                    <p><?php the_field('address_1'); ?><br>
                    <?php if (get_field('address_2') != '') { the_field('address_2'); echo '<br>'; } ?>
                    <?php the_field('city'); echo ', '; the_field('state'); echo ' '; the_field('zip'); ?><br>
                    <?php if (get_field('phone') != '') { the_field('phone'); echo '<br>'; } ?>
                    <?php if (get_field('web') != '') { echo '<a href="'; the_field('web'); echo '">'; the_field('web'); echo '</a>'; } ?>
                    </p>
                    <?php if (get_field('hours') != '') { echo '<p>'; the_field('hours'); echo '</p>'; } ?>
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="closer" onclick="document.getElementById(activeModal).setAttribute('class','lg_box hidden');document.getElementById('backdrop').setAttribute('class','modal-backdrop hidden');"></div>
</div>
